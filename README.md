# **MGO Config Bundle**


## **Installation**

#### Installation with composer
```sh
composer config repositories.mgoconsulting/config-bundle git https://bitbucket.org/mgoconsulting/config-bundle.git
composer require mgoconsulting/config-bundle
```
#### Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\ConfigBundle\MgoConfigBundle::class => ['all' => true],
];
```
#### Enable the extension

In *config/packages/mgo_config.yaml*
```yaml
mgo_config:
    api:
        values_mapping:
            'FALSE': false
            'TRUE': true
            'NULL': null
    config_definitions:
        colonnes:
            class: Mgo\VenteBundle\Configuration\ExampleConfiguration

```
#### Enable API routes

In *config/routes.yaml* add
```yaml
...
config_bundle_api_routes:
    prefix: /api # prefix it like you want
    resource: '@MgoConfigBundle/Resources/config/routes.yml'
```

## **API documentation**


### *[GET] /configuration/value/{name}/{key}*
Returns config *{name}* values with key *{key}* in JSON format.
If the config key *{key}* does not exists in the database, it returns the default values.

**Request**

| Parameter | Required  | Description                                       |
|-----------|-----------|---------------------------------------------------|
| name      | YES       | config name *(must match regex [a-z0-9_\-]+)*     |
| key       | YES       | unique key name *(must match regex [a-z0-9_\-]+)* |

**Responses**

| Code  | Description                                          |
|-------|------------------------------------------------------|
| 200   | Success, returns JSON config values                  |
| 400   | Configuration error (from Symfony Config Component)  |
| 404   | Configuration name not found                         |
| 500   | Fatal server error                                   |

**Example**
```bash
curl -X GET "/configuration/value/config-name/key-name" \
     -H "accept: application/json"
```



### *[GET] /configuration/has/{name}/{key}*
Check if config *{name}* with key *{key}* exists.

**Request**

| Parameter | Required  | Description                                       |
|-----------|-----------|---------------------------------------------------|
| name      | YES       | config name *(must match regex [a-z0-9_\-]+)*     |
| key       | YES       | unique key name *(must match regex [a-z0-9_\-]+)* |

**Responses**

| Code  | Description                                          |
|-------|------------------------------------------------------|
| 200   | Success, returns JSON config values                  |
| 400   | Configuration error (from Symfony Config Component)  |
| 404   | Configuration name not found                         |
| 500   | Fatal server error                                   |

**Example**
```bash
curl -X GET "/configuration/has/config-name/key-name" \
     -H "accept: application/json"
```

                               

### *[GET] /configuration/default/{name}*
Returns the default config *{name}* values in JSON format.

**Request**

| Parameter | Required  | Description                                       |
|-----------|-----------|---------------------------------------------------|
| name      | YES       | config name *(must match regex [a-z0-9_\-]+)*     |
| key       | YES       | unique key name *(must match regex [a-z0-9_\-]+)* |

**Responses**

| Code  | Description                                          |
|-------|------------------------------------------------------|
| 200   | Success, returns JSON config values                  |
| 400   | Configuration error (from Symfony Config Component)  |
| 404   | Configuration name not found                         |
| 500   | Fatal server error                                   |

**Example**
```bash
curl -X GET "/configuration/default/config-name" \
     -H "accept: application/json"
```



### *[POST] /configuration/value/{name}/{key}*
Save config *{name}* values with key *{key}*.

**Request**

| Parameter | Required  | Description                                       |
|-----------|-----------|---------------------------------------------------|
| name      | YES       | config name *(must match regex [a-z0-9_\-]+)*     |
| key       | YES       | unique key name *(must match regex [a-z0-9_\-]+)* |

**Post Parameter**
Array of config values to save

**Responses**

| Code  | Description                                          |
|-------|------------------------------------------------------|
| 200   | Success, returns JSON config values                  |
| 400   | Configuration error (from Symfony Config Component)  |
| 404   | Configuration name not found                         |
| 500   | Fatal server error                                   |

**Example**
```bash
curl -X POST "/configuration/value/config-name/key-name" \
     -H "accept: application/json" \
     -d "{"key1": true, "key2": [1,2,3]}"
```

