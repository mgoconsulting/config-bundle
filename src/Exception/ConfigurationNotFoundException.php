<?php

namespace Mgo\ConfigBundle\Exception;

class ConfigurationNotFoundException extends \Exception implements ConfigurationExceptionInterface
{
}
