<?php

namespace Mgo\ConfigBundle\Exception;

class UserNotFoundException extends \Exception implements ConfigurationExceptionInterface
{
}
