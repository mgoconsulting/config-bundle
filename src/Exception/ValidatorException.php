<?php

namespace Mgo\ConfigBundle\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidatorException extends \Exception implements ConfigurationExceptionInterface
{
    public function __construct(ConstraintViolationListInterface $violations, $code = null, $previous = null)
    {
        $errors = ['Validation errors:'];
        /* @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $errors[] = \sprintf(
                ' %s: Value "%s" is invalid. %s',
                $violation->getPropertyPath(),
                $violation->getInvalidValue(),
                $violation->getMessage()
            );
        }

        parent::__construct(implode(PHP_EOL, $errors), $code, $previous);
    }
}
