<?php

namespace Mgo\ConfigBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Mgo Config Extension.
 */
class MgoConfigExtension extends Extension
{
    const ALIAS = 'mgo_config';

    public function getAlias()
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        // load yaml files
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        // set config in parameters
        $container->setParameter('mgo_config.configuration', $config);
    }
}
