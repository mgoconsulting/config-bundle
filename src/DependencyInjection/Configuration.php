<?php

namespace Mgo\ConfigBundle\DependencyInjection;

use Mgo\ConfigBundle\Configuration\AbstractConfiguration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_config');
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_config');
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->children()
                ->arrayNode('user')
                    ->isRequired()
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->variableNode('class')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->variableNode('referenced_column_name')
                            ->defaultValue('id')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('config_definitions')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class')
                                ->validate()
                                    // check config class exists
                                    ->ifTrue(function ($class) {
                                        return !\class_exists($class);
                                    })
                                    ->thenInvalid('Configuration class %s does not exists')
                                ->end()
                                ->validate()
                                    // check config class using abstract class AbstractConfiguration
                                    ->ifTrue(function ($class) {
                                        return !is_subclass_of($class, AbstractConfiguration::class);
                                    })
                                    ->thenInvalid(
                                        'Configuration class %s does not extends abstract class '
                                        .AbstractConfiguration::class
                                    )
                                ->end()
                                ->isRequired()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
