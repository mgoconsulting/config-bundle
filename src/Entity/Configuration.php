<?php

namespace Mgo\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *     name="configuration",
 *     indexes={
 *         @ORM\Index(name="idx", columns={"config_name", "key_name", "owner_id"}),
 *         @ORM\Index(name="no_owner_idx", columns={"config_name", "key_name"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 * @UniqueEntity(
 *     fields={"configName", "keyName", "owner"},
 *     ignoreNull=true,
 *     message="This config already in use"
 * )
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="config_name", type="string", nullable=false)
     * @Assert\Length(max=50)
     * @Assert\NotNull()
     */
    private $configName;

    /**
     * @ORM\Column(name="key_name", type="string", nullable=false)
     * @Assert\Length(max=50)
     * @Assert\NotNull()
     */
    private $keyName;

    /**
     * @ORM\ManyToOne(targetEntity="Mgo\ConfigBundle\Entity\UserInterface")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true)
     */
    private $owner = null;

    /**
     * @ORM\Column(name="config", type="json", nullable=false)
     */
    private $config = [];

    public function __construct($configName, $keyName, $owner = null)
    {
        $this->configName = $configName;
        $this->keyName = $keyName;
        $this->owner = $owner;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConfigName(): string
    {
        return $this->configName;
    }

    public function getKeyName(): string
    {
        return $this->keyName;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig($config): self
    {
        $this->config = $config;

        return $this;
    }
}
