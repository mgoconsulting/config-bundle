<?php

namespace Mgo\ConfigBundle\Service;

use Mgo\ConfigBundle\Configuration\AbstractConfiguration;
use Mgo\ConfigBundle\Entity\Configuration;
use Mgo\ConfigBundle\Exception\ConfigurationNotFoundException;
use Mgo\ConfigBundle\Exception\UserNotFoundException;
use Mgo\ConfigBundle\Exception\ValidatorException;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Configurator
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $em;

    /** @var \Symfony\Component\Validator\Validator\ValidatorInterface */
    private $validator;

    /** @var array */
    private $bundleConfig;

    /** @var object */
    private $user;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine')->getManagerForClass(Configuration::class);
        $this->validator = $container->get('validator');
        $this->bundleConfig = $container->getParameter('mgo_config.configuration');

        if ($container->has('security.token_storage')) {
            $token = $container->get('security.token_storage')->getToken();
            if ($token && \is_object($user = $token->getUser())) {
                $this->user = $user;
            }
        }
    }

    public function setUser(object $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function default(string $configName)
    {
        return $this->getConfigurationDefaultValue($configName);
    }

    public function get(string $configName, string $keyName)
    {
        $configuration = $this->findConfiguration($configName, $keyName);

        if (!$configuration) {
            $configName = \sprintf('Configuration \'%s\' with key \'%s\' was not found', $configName, $keyName);
            throw new ConfigurationNotFoundException($configName, 404);
        }

        return $configuration->getConfig() + $this->getConfigurationDefaultValue($configName);
    }

    public function has(string $configName, string $keyName): bool
    {
        return (bool) $this->findConfiguration($configName, $keyName);
    }

    public function set(string $configName, string $keyName, array $values): array
    {
        $configuration = $this->findConfiguration($configName, $keyName);
        // create if configuration does not exists
        if (!$configuration) {
            if ($this->isUserSpecific($configName) && !$this->user) {
                throw new UserNotFoundException("Configuration '{$configName}' needs a authenticated user");
            }
            $configuration = new Configuration($configName, $keyName, $this->user);
        }

        $values = $this->getConfigurationDefaultValue($configName, $values);
        $configuration->setConfig($values);

        $this->em->persist($configuration);
        $violations = $this->validator->validate($configuration);
        if ($violations->count()) {
            throw new ValidatorException($violations);
        }
        $this->em->flush();

        return $values;
    }

    private function getConfigurationDefaultValue(string $configName, array $overrides = []): array
    {
        $this->validateConfigurationExists($configName);
        $configuration = $this->bundleConfig['config_definitions'][$configName];

        $processed = (new Processor())->processConfiguration(
            new $configuration['class'](),
            [[AbstractConfiguration::VALUES_KEY => $overrides]]
        );

        return $processed[AbstractConfiguration::VALUES_KEY];
    }

    private function findConfiguration(string $configName, string $keyName)
    {
        $criterias = [
            'configName' => $configName,
            'keyName' => $keyName,
        ];

        if ($this->isUserSpecific($configName)) {
            if (!$this->user) {
                throw new UserNotFoundException("Configuration '{$configName}' needs a authenticated user");
            }
            $criterias['owner'] = $this->user->getId();
        }

        return $this->em->getRepository(Configuration::class)->findOneBy($criterias);
    }

    private function validateConfigurationExists(string $configName): void
    {
        if (!isset($this->bundleConfig['config_definitions'][$configName])) {
            $configName = \sprintf('Configuration \'%s\' was not found', $configName);
            throw new ConfigurationNotFoundException($configName);
        }
    }

    private function isUserSpecific(string $configName): bool
    {
        $this->validateConfigurationExists($configName);

        return \call_user_func([
            $this->bundleConfig['config_definitions'][$configName]['class'],
            'isUserSpecific',
        ]);
    }
}
