<?php

namespace Mgo\ConfigBundle;

use Mgo\ConfigBundle\DependencyInjection\MgoConfigExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Config Bundle.
 */
class MgoConfigBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass()
    {
        return MgoConfigExtension::class;
    }
}
