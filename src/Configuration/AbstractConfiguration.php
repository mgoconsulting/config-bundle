<?php

namespace Mgo\ConfigBundle\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

abstract class AbstractConfiguration implements ConfigurationInterface
{
    const VALUES_KEY = 'values';

    abstract public static function isUserSpecific(): bool;

    abstract protected function buildConfig(NodeBuilder $node): void;

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_config');
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_config');
        }

        $node = new ArrayNodeDefinition(self::VALUES_KEY);

        $children = $node
            ->addDefaultsIfNotSet()
            ->children();
        $this->buildConfig($children);
        $node->end();

        $root
            ->addDefaultsIfNotSet()
            ->append($node);

        return $treeBuilder;
    }
}
