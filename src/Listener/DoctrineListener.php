<?php

namespace Mgo\ConfigBundle\Listener;

use Doctrine\Common\EventArgs;
use Mgo\ConfigBundle\Entity\Configuration;
use Mgo\ConfigBundle\Entity\UserInterface;

class DoctrineListener
{
    /** @var array */
    private $bundleConfig;

    public function __construct(array $bundleConfig)
    {
        $this->bundleConfig = $bundleConfig;
    }

    public function loadClassMetadata(EventArgs $eventArgs)
    {
        $cm = $eventArgs->getClassMetadata();

        if (Configuration::class === $cm->name
            && UserInterface::class === $cm->associationMappings['owner']['targetEntity']) {
            $config = $this->bundleConfig;
            $newMapping = $cm->associationMappings['owner'];
            $newMapping['targetEntity'] = $config['user']['class'];
            $newMapping['joinColumns'][0]['referencedColumnName'] = $config['user']['referenced_column_name'];
            unset($cm->associationMappings['owner']);
            $cm->mapManyToOne($newMapping);
        }
    }
}
