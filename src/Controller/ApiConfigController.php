<?php

namespace Mgo\ConfigBundle\Controller;

use Mgo\ConfigBundle\Exception\ConfigurationExceptionInterface;
use Mgo\ConfigBundle\Exception\UserNotFoundException;
use Mgo\ConfigBundle\Service\Configurator;
use Symfony\Component\Config\Definition\Exception\Exception as SfConfigException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiConfigController
{
    /** @var array */
    private $configurator;

    /** @var string */
    private $env;

    public function __construct(Configurator $configurator, string $env)
    {
        $this->configurator = $configurator;
        $this->env = $env;
    }

    public function valueAction(Request $request): JsonResponse
    {
        return $this->configurator($request, function ($name, $key) use ($request) {
            // action for GET method
            if ($request->isMethod(Request::METHOD_GET)) {
                if ($this->configurator->has($name, $key)) {
                    $data = $this->configurator->get($name, $key);
                } else {
                    $data = $this->configurator->default($name, $key);
                }

                return $data;
            }

            // action for POST method
            $content = $request->getContent();
            if (!$content) {
                throw new SfConfigException('JSON Content is empty', 400);
            }
            $json = @\json_decode($content, true);
            if (!is_array($json)) {
                throw new SfConfigException('JSON '.json_last_error_msg(), 400);
            }
            $this->configurator->set($name, $key, $json);

            return $this->configurator->get($name, $key);
        });
    }

    public function defaultAction(Request $request): JsonResponse
    {
        return $this->configurator($request, function ($name, $key) {
            return $this->configurator->default($name, $key);
        });
    }

    public function hasAction(Request $request): JsonResponse
    {
        return $this->configurator($request, function ($name, $key) {
            return [
                'exists' => $this->configurator->has($name, $key),
            ];
        });
    }

    private function configurator(Request $request, callable $callback): JsonResponse
    {
        $name = $request->attributes->get('name');
        $key = $request->attributes->get('key');

        try {
            $status = 200;
            $data = $callback($name, $key);
        } catch (UserNotFoundException $e) {
            $status = 401;
            $data = ['error' => \sprintf('Access denied: %s', $e->getMessage())];
        } catch (ConfigurationExceptionInterface $e) {
            $status = 404;
            $data = ['error' => \sprintf('Config error: %s', $e->getMessage())];
        } catch (SfConfigException $e) {
            $status = 400;
            $data = ['error' => \sprintf('Invalid values: %s', $e->getMessage())];
        } catch (\Exception $e) {
            $status = 500;
            $error = 'Internal server error';
            if ('prod' !== $this->env) {
                $error = (string) $e;
            }
            $data = ['error' => \sprintf('Fatal error: %s', $error)];
        }

        return new JsonResponse($data, $status);
    }
}
