<?php

namespace Test\Service;

use Mgo\ConfigBundle\Exception;
use Mgo\ConfigBundle\Service\Configurator;
use Test\TestCase;

class ConfiguratorTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        // create Configurator instance
        self::$configurator = self::$kernel->getContainer()->get('mgo.config_bundle.configurator');
    }

    public function testHasFailureConfigNotFound(): void
    {
        $this->expectException(Exception\ConfigurationNotFoundException::class);
        $this->expectExceptionMessage("Configuration 'config name does not exists");

        $this->assertFalse(self::$configurator->has('config name does not exists', 'a'));
    }

    public function testHas(): void
    {
        $this->assertFalse(self::$configurator->has('test1', 'whatever'));
        $this->assertTrue(self::$configurator->has('test1', 'a'));
        $this->assertTrue(self::$configurator->has('test1', 'b'));
    }

    public function testGetFailureConfigNotFound(): void
    {
        $this->expectException(Exception\ConfigurationNotFoundException::class);
        $this->expectExceptionMessage("Configuration 'config name does not exists");

        self::$configurator->get(
            'config name does not exists',
            'whatever'
        );
    }

    public function testGetFailureConfigKeyNotFound(): void
    {
        $this->expectException(Exception\ConfigurationNotFoundException::class);
        $this->expectExceptionMessage("Configuration 'test1' with key 'key does not exists' was not found");

        self::$configurator->get(
            'test1',
            'key does not exists'
        );
    }

    public function testGetSuccess(): void
    {
        $this->assertEquals(
            self::$configurator->get('test1', 'a'),
            [
                'bool_default_true' => false,
                'array_prototype' => [],
            ]
        );

        $this->assertEquals(
            self::$configurator->get('test1', 'b'),
            [
                'bool_default_true' => true,
                'array_prototype' => ['scalar1', 'scalar2'],
            ]
        );
    }

    public function testSetFailureConfigNotFound(): void
    {
        $this->expectException(Exception\ConfigurationNotFoundException::class);
        $this->expectExceptionMessage("Configuration 'config name does not exists");

        self::$configurator->set(
            'config name does not exists',
            'whatever',
            ['bool_default_true' => true]
        );
    }

    public function testSetFailureValidation(): void
    {
        $this->expectException(Exception\ValidatorException::class);
        $this->expectExceptionMessage('keyName: Value "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" is invalid');
        $this->expectExceptionMessage('This value is too long');

        self::$configurator->set(
            'test1',
            \str_repeat('y', 51),
            ['bool_default_true' => true]
        );
    }

    public function testSet(): void
    {
        $keyName = \uniqid();

        $values = self::$configurator->set(
            'test1',
            $keyName,
            ['bool_default_true' => true]
        );

        $this->assertEquals(
            $values,
            [
                'bool_default_true' => true,
                'array_prototype' => [],
            ]
        );
    }
}
