<?php

namespace Test;

use Mgo\ConfigBundle\Configuration\AbstractConfiguration;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

class Test1Configuration extends AbstractConfiguration
{
    public static function isUserSpecific(): bool
    {
        return false;
    }

    protected function buildConfig(NodeBuilder $node): void
    {
        $node
            ->booleanNode('bool_default_true')
                ->defaultTrue()
            ->end()
            ->arrayNode('array_prototype')
                ->scalarPrototype()->end()
            ->end();
    }
}
