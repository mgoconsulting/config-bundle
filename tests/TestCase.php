<?php

namespace Test;

use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Persistence\ObjectRepository;
use Mgo\ConfigBundle\Entity\Configuration;
use Mgo\ConfigBundle\Service\Configurator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class TestCase extends WebTestCase
{
    /** @var \Doctrine\Persistence\ManagerRegistry */
    protected static $doctrine;

    /** @var Configurator */
    protected static $configurator;

    /** @var \Symfony\Bundle\FrameworkBundle\KernelBrowser */
    protected static $client;

    /** @var Configurator */
    private static $fixtures = [
        'test1' => [ // config name
            'a' => [ // key
                'config' => [
                    'bool_default_true' => false,
                ],
            ],
            'b' => [
                'config' => [
                    'array_prototype' => ['scalar1', 'scalar2'],
                ],
            ],
        ],
    ];

    public static function setUpBeforeClass(): void
    {
        // client
        self::$client = static::createClient(['debug' => self::isDebug()]);
        // boot test kernel
        static::bootKernel(['debug' => self::isDebug()]);
        // get doctrine services
        self::$doctrine = self::$kernel->getContainer()->get('doctrine');

        // create db
        self::buildDb();

        // load data fixtures from self::$fixtures
        self::loadDataFixtures();
    }

    private static function buildDb(): void
    {
        $dbPath = sys_get_temp_dir().'/mgo-config-test.db';
        @unlink($dbPath);

        $em = self::$doctrine->getManager();
        $connection = $em->getConnection();
        $database = $connection->getDatabase();

        $schemaManager = $connection->getSchemaManager();
        $schemaManager->dropDatabase($database);
        $schemaManager->createDatabase($database);

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema($em->getMetadataFactory()->getAllMetadata());
    }

    protected static function isDebug(): bool
    {
        return \in_array('--debug', $_SERVER['argv'] ?? []);
    }

    protected function getConfigurationRepo(): ObjectRepository
    {
        return self::$doctrine->getRepository(Configuration::class);
    }

    private static function loadDataFixtures(): void
    {
        // load fixtures if exists
        if (self::$fixtures) {
            $em = self::$doctrine->getManager();
            // property accessor instance
            $builder = PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex();

            if (\method_exists($builder, 'enableExceptionOnInvalidPropertyPath')) {
                $builder->enableExceptionOnInvalidPropertyPath();
            }
            $accessor = $builder->getPropertyAccessor();

            // create somt existsing config
            foreach (self::$fixtures as $configName => $fixtures) {
                foreach ($fixtures as $keyName => $fields) {
                    $configuration = new Configuration($configName, $keyName);
                    foreach ($fields as $path => $value) {
                        $accessor->setValue($configuration, $path, $value);
                    }
                    $em->persist($configuration);
                }
            }
            $em->flush();
        }
    }
}
