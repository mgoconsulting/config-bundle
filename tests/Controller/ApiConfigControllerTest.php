<?php

namespace Test;

use Mgo\ConfigBundle\Entity\Configuration;

class ApiConfigControllerTest extends TestCase
{
    public function getData()
    {
        return [
            // Configuration does not exists
            [404, ['error' => "Config error: Configuration 'whatever' was not found"], 'whatever', 'whatever'],
            // config with key not in db, return default
            [
                200,
                [
                    'bool_default_true' => true,
                    'array_prototype' => [],
                ],
                'test1',
                'whatever',
            ],
            // config key a
            [
                200,
                [
                    'bool_default_true' => false,
                    'array_prototype' => [],
                ],
                'test1',
                'a',
            ],
            // config key b
            [
                200,
                [
                    'bool_default_true' => true,
                    'array_prototype' => ['scalar1', 'scalar2'],
                ],
                'test1',
                'b',
            ],
        ];
    }

    /**
     * @dataProvider getData
     */
    public function testGet(
        int $expectedStatusCode,
        array $expectedContent,
        string $configName,
        string $keyName
    ) {
        $this->doAssert(
            'GET',
            "/configuration/value/{$configName}/{$keyName}",
            $expectedStatusCode,
            $expectedContent
        );
    }

    public function hasData()
    {
        return [
            // Configuration does not exists
            [404, ['error' => "Config error: Configuration 'whatever' was not found"], 'whatever', 'whatever'],
            // config key whatever does not exists
            [200, ['exists' => false], 'test1', 'whatever'],
            // config key whatever exists
            [200, ['exists' => true], 'test1', 'a'],
        ];
    }

    /**
     * @dataProvider hasData
     */
    public function testHas(
        int $expectedStatusCode,
        array $expectedContent,
        string $configName,
        string $keyName
    ) {
        $this->doAssert(
            'GET',
            "/configuration/has/{$configName}/{$keyName}",
            $expectedStatusCode,
            $expectedContent
        );
    }

    public function defaultData()
    {
        return [
            // Configuration does not exists
            [404, ['error' => "Config error: Configuration 'whatever' was not found"], 'whatever'],
            // config with key not in db, return default
            [
                200,
                $default = [
                    'bool_default_true' => true,
                    'array_prototype' => [],
                ],
                'test1',
            ],
        ];
    }

    /**
     * @dataProvider defaultData
     */
    public function testDefault(
        int $expectedStatusCode,
        array $expectedContent,
        string $configName
    ) {
        $this->doAssert(
            'GET',
            "/configuration/default/{$configName}",
            $expectedStatusCode,
            $expectedContent
        );
    }

    public function setData()
    {
        return [
            'JSON Empty' => [
                400,
                ['error' => 'Invalid values: JSON Content is empty'],
                'whatever',
                'whatever',
                '',
            ],
            'JSON syntax error' => [
                400,
                ['error' => 'Invalid values: JSON Syntax error'],
                'whatever',
                'whatever',
                'SYNTAX ERROR JSON',
            ],
            'Config whatever does not exists' => [
                404,
                ['error' => "Config error: Configuration 'whatever' was not found"],
                'whatever',
                'whatever',
                '{"values": 1}',
            ],
            'Config test1 exists but wrong values' => [
                400,
                ['error' => 'Invalid values: Unrecognized option "values" under "mgo_config.values". '.
                            'Available options are "array_prototype", "bool_default_true".', ],
                'test1',
                'whatever',
                '{"values": "WRONG"}',
            ],
            'Config test1 set ok bool_default_true' => [
                200,
                [
                    'bool_default_true' => false,
                    'array_prototype' => [],
                ],
                'test1',
                \uniqid(),
                '{"bool_default_true": false}',
            ],
            'Config test1 set ok array_prototype' => [
                200,
                [
                    'bool_default_true' => true,
                    'array_prototype' => [1, 2, 3],
                ],
                'test1',
                \uniqid(),
                '{"array_prototype": [1, 2, 3]}',
            ],
        ];
    }

    /**
     * @dataProvider setData
     */
    public function testSet(
        int $expectedStatusCode,
        array $expectedContent,
        string $configName,
        string $keyName,
        string $content
    ) {
        $this->doAssert(
            'POST',
            "/configuration/value/{$configName}/{$keyName}",
            $expectedStatusCode,
            $expectedContent,
            $content
        );
    }

    public function testSetUnicity()
    {
        $key = \uniqid('test_set_failures');

        $initialCount = count($this->getConfigurationRepo()->findAll());

        // create first set a valid values
        $this->doAssert(
            'POST',
            "/configuration/value/test1/{$key}",
            200,
            ['array_prototype' => [1, 2, 3], 'bool_default_true' => true],
            '{"array_prototype": [1, 2, 3]}'
        );
        $this->assertCount($initialCount + 1, $this->getConfigurationRepo()->findAll());

        // re-set values
        $this->doAssert(
            'POST',
            "/configuration/value/test1/{$key}",
            200,
            ['array_prototype' => [999999], 'bool_default_true' => true],
            '{"array_prototype": [999999]}'
        );
        $this->assertCount($initialCount + 1, $this->getConfigurationRepo()->findAll());
    }

    /**
     * @dataProvider hasData
     */
    private function doAssert(
        string $method,
        string $path,
        int $expectedStatusCode,
        array $expectedContent,
        string $content = null
    ) {
        self::$client->request($method, $path, [], [], [], $content);
        $response = self::$client->getResponse();
        // assert status code
        $this->assertEquals($expectedStatusCode, $response->getStatusCode(), $response->getContent());
        // assert JSON content
        $content = $response->getContent();
        $this->assertJson($content, "content: {$content}");
        $array = \json_decode($content, true);
        $this->assertEquals($expectedContent, $array);
    }
}
