<?php

namespace Test;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

// phpcs:disable
require __DIR__.'/../vendor/autoload.php';
// phpcs:enable

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles()
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Mgo\ConfigBundle\MgoConfigBundle(),
        ];
    }

    public function welcomeAction()
    {
        return new Response('hello');
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader)
    {
        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);
        $builder->loadFromExtension('doctrine', [
            'dbal' => [
                'default_connection' => 'test',
                'connections' => [
                    'test' => [
                        'driver' => 'pdo_sqlite',
                        'path' => sys_get_temp_dir().'/mgo-config-test.db',
                    ],
                ],
            ],
            'orm' => [
                'mappings' => [
                    'src' => [
                        'type' => 'annotation',
                        'prefix' => 'Mgo\ConfigBundle\Entity',
                        'dir' => \dirname(__DIR__).'/src/Entity',
                    ],
                    'tests' => [
                        'type' => 'annotation',
                        'prefix' => 'Test\Entity',
                        'dir' => __DIR__.'/Entity',
                    ],
                ],
            ],
        ]);
        $builder->loadFromExtension('mgo_config', [
            'user' => [
                'class' => \Test\Entity\User::class,
            ],
            'config_definitions' => [
                'test1' => [
                    'class' => \Test\Test1Configuration::class,
                ],
            ],
        ]);
    }

    protected function configureRoutes(RoutingConfigurator $routes)
    {
        $routes->add('/', 'kernel::welcomeAction');
        $routes->import(\dirname(__DIR__).'/src/Resources/config/routes.yml');
    }
}
